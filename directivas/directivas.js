app.directive('header', function() {
    return {
        templateUrl: 'vistas/directivas/header.html'
    };
});
app.directive('footer', function() {
    return {
        templateUrl: 'vistas/directivas/footer.html'
    };
});