app.controller('homeCtl', function($scope, $rootScope, $location) {

    $scope.socialNet = [
        {image: "fab fa-facebook-f"},
        {image: "fab fa-twitter"},
        {image: "fab fa-linkedin-in"}
    ];

    $scope.nav = ["Nivel 1", "Nivel 2", "Nivel 3"];

    $scope.contentOne = {
        title: 'Construyamos juntos el futuro.',
        content1: 'El ahorro pensional debe ser del 16% sobre sus ingresos mensuales. Estos aportes irán a una cuenta de ahorro individual a tu nombre, en la que generarán rendimientos de acuerdo a las condiciones del mercado financiero.',
        content2: 'La Superintendencia Financiera exige para tu ahorro pensional una garantía mínima de rentabilidad, para asegurarse que tus recursos no corran riesgos.',
        content3: 'Al afiliarte a nuestro fondo, obtendrás cobertura de acuerdo a los requisitos de ley por: invalidez, sobrevivencia o vejez.'
    };

    $scope.beneficios = [];

    var card1 = { 
        title: 'Alianzas para afiliados',
        text: 'Descubre aquí los beneficios que tienes como afiliado a Colfondos.',
        boton: 'Conoce más',
        image: 'Afiliados'
    }
    var card2 = { 
        title: 'Beneficios del Mes',
        text: 'Encuentra aquí los privilegios que tienes por pertenecer a Colfondos.',
        boton: 'Conoce más',
        image: 'beneficios-club-colfondos-editada'
    }
    var card3 = { 
        title: 'Alianzas para empresas',
        text: 'Descubre aquí los beneficios que tienes para tu empresa.',
        boton: 'Conoce más',
        image: 'Empresa'
    }

    $scope.beneficios.push(card1, card2, card3);
});